"""music_library_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from core.views import Index, DeleteTrack, AddTrack, EditTrack, SearchTrack

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Index.as_view()),
    path('delete_track/<int:pk>', DeleteTrack.as_view()),
    path('add_track/', AddTrack.as_view()),
    path('edit_track/<int:pk>/', EditTrack.as_view()),
    path('search_track/', SearchTrack.as_view()),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('accounts/', include('django.contrib.auth.urls'))
]
