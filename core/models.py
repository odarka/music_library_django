from django.db import models


class Track(models.Model):
    performer = models.CharField(max_length=255)
    track = models.CharField(max_length=255)
    duration = models.CharField(max_length=5)

