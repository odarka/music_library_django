from re import match

from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import DeleteView, CreateView, UpdateView
from django.views.generic.base import TemplateView


from core.models import Track


def check_duration(duration):
    if not match(r'^[1-9]\d*\:[0-5][0-9]$', duration):
        return "enter duration like 00:00"
    else:
        return ''


class Index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        tracks = Track.objects.all()
        return {
            'tracks': tracks
        }


class DeleteTrack(DeleteView):
    model = Track
    success_url = '/'


class AddTrack(CreateView):
    template_name = 'add_track.html'
    model = Track
    fields = ('performer', 'track', 'duration')

    def get_context_data(self, **kwargs):
        return {
            'duration_error': '',
            'performer': '',
            'track': '',
            'duration': ''
        }

    def form_valid(self, form):
        duration = form.cleaned_data['duration']
        duration_error = check_duration(duration)
        performer = form.cleaned_data['performer']
        track = form.cleaned_data['track']
        if duration_error:
            return render(self.request, self.template_name, {
                'duration_error': duration_error,
                'performer': performer,
                'track': track,
                'duration': ''
            })
        else:
            track = form.save(commit=True)
            track.save()
            return redirect('/')


class EditTrack(UpdateView):
    model = Track
    fields = ('performer', 'track', 'duration')
    template_name = 'edit_track.html'

    def form_valid(self, form):
        track_id = self.kwargs.get('pk', '')
        duration_error = check_duration(form.cleaned_data['duration'])
        track = Track.objects.filter(id=track_id).get()
        if duration_error:
            return render(self.request, self.template_name, {
                'duration_error': duration_error,
                'track': track,
            })
        else:
            track_save = form.save(commit=True)
            track_save.save()
            return redirect('/')


class SearchTrack(TemplateView):
    model = Track
    template_name = 'search_track.html'

    def get_context_data(self, **kwargs):
        search_query = self.request.GET.get('search', '')
        if search_query != '':
            search_query_lst = search_query.split(' ')
            likes = []
            for word in search_query_lst:
                likes.append(Q(track__contains=word))
                likes.append(Q(performer__contains=word))

            tracks = Track.objects.filter(
                *likes,
                _connector='OR'
            )

        else:
            tracks = []

        return {
            'tracks': tracks
        }
